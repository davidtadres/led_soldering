# LED soldering

This repository was created for a lesson in soldering for students of 
varying experience.

The described project is very basic and should be accessible for 
beginners.

This project is inspired by this publication [Optogenetics in the 
teaching laboratory: using channelrhodopsin-2 to study the neural 
basis of behavior and synaptic physiology in Drosophila 
](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3276384/).

## Components

- An LED, for example the [Cree XLamp XP-E2
](https://www.ledsupply.com/leds/cree-xlamp-xp-e2-color-high-power-led-star)
in any color you like. **These LEDs are very bright - do not look directly
into them!**
- A [lens](https://www.ledsupply.com/led-optics/10124-carclo-lens-frosted-narrow-spot-led-optic) 
and a [lensholder](https://www.ledsupply.com/led-optics/10496-carclo-lens-holder)
for the LED: 
- A [heatsink](https://www.ledsupply.com/led-heatsinks/heatsink-finned-with-adhesive-tape)
 for the LED as it would get too hot otherwise
- A MiniPuck to control the current the LED can draw. Make sure to get 
a pinned version, for example [this one: 
F004-D-P-700](https://www.ledsupply.com/led-drivers/minipuck-constant-current-led-driver)
- This [solderable mini breadboard](https://www.sparkfun.com/products/12702)
- Two male to male jumper wires, e.g. from [here](https://www.sparkfun.com/products/14284)
- A manual [switch](https://www.ledsupply.com/accessories/on-off-switch-toggle-with-solder-lug-terminals)
to turn the LED on or off
- A power source - The easiest (and safest) option is to get [this 9V 
Battery Clip connector](https://www.ledsupply.com/accessories/9v-battery-connector-with-plug)
and a 9V battery
- Two male and two female [Barrel 
Plugs](https://www.amazon.com/2-1x5-5mm-Pigtails-Security-Rearview-Application/dp/B071GV5XDD/ref=pd_day0_hl_60_3/143-1881930-5573125?_encoding=UTF8&pd_rd_i=B071GV5XDD&pd_rd_r=74e52552-8718-11e9-b1ac-6bd4bdce431e&pd_rd_w=oddiv&pd_rd_wg=YxNMs&pf_rd_p=ad07871c-e646-4161-82c7-5ed0d4c85b07&pf_rd_r=MXPEEFC04PYTR04R30Q1&psc=1&refRID=MXPEEFC04PYTR04R30Q1)
- 6x M3x10 [screw](https://www.mcmaster.com/91290A115) and [nut](https://www.mcmaster.com/90591a250) 

### Tools

- [Soldering iron](https://www.amazon.com/Hakko-FX888D29BY-ESD-Safe-Digital-Soldering/dp/B00OSM27T8/ref=sr_1_9?keywords=soldering+station+hakko&qid=1560923541&s=gateway&sr=8-9)
- [Smoke Absorber](https://www.amazon.com/Hakko-FA400-04-Bench-ESD-Safe-Absorber/dp/B00FZPSEY4/ref=sr_1_1?keywords=hakko+fan&qid=1560923688&s=gateway&sr=8-1)
- [Solder](https://www.amazon.com/WYCTIN-Solder-Electrical-Solderding-0-11lbs/dp/B071G1J3W6/ref=sr_1_5?s=hi&ie=UTF8&qid=1527741062&sr=1-5&keywords=solder)
- [Wire Stripper](https://www.amazon.com/dp/B07FP1XXQM/ref=sspa_dk_detail_1?psc=1&pd_rd_i=B07FP1XXQM)
- [Heat Gun](https://www.amazon.com/Electric-Temperature-Industrial-Portable-Remover/dp/B01MCUJ4TY/ref=sr_1_33?keywords=heat+gun&qid=1560923726&s=gateway&sr=8-33)
- [Heat Shrinking Tubing](https://www.amazon.com/Ginsco-580-pcs-Assorted-Sleeving/dp/B01MFA3OFA/ref=pd_bxgy_60_img_2/136-5759624-8945311?_encoding=UTF8&pd_rd_i=B01MFA3OFA&pd_rd_r=31c4ed15-9257-11e9-968c-571b12b66399&pd_rd_w=284yP&pd_rd_wg=3H2PA&pf_rd_p=a2006322-0bc0-4db9-a08e-d168c18ce6f0&pf_rd_r=J9EZEZ3J73ZX606B6N93&psc=1&refRID=J9EZEZ3J73ZX606B6N93)
- [Hex Wrench](https://www.amazon.com/dp/B071NBGBQD/ref=sspa_dk_detail_2?psc=1&pd_rd_i=B071NBGBQD&pd_rd_wg=kmByc&pd_rd_r=H240CYDM209AMVWTG5PS&pd_rd_w=9dkZE)

## 3D Printing

You may find the 3D printer files in the 'STL_files' folder. You will
need to print:
 - 1x Casing 
 - 1x Casing_Closure
 - 2x LED_Holder
 - 2x LED_Connector

For best results use a printer capable of using PVA as support material!

 ## Wiring Diagram
 
![Wiring Diagrame](wiring_diagram/Wiring_Diagram_schem.png)
 
 ## "Dry testing"
 
 When building a circuit it is often good to first build it in a 
 reversible manner to make sure everything functions as expected.
 
 The breadboard is great for that (TODO)
 
 ## Soldering
 
 After making sure the circuit is indeed working as expected it's 
 soldering time!
 
 Use a high quality soldering iron at ~750F. Don't forget to turn 
 the smoke absorber on. 
 
1) First make sure you have all the components and the necessary 
[tools](#necessary-tools): 
![All components](pics/1_components.JPG)

2) Place the BuckPuck on the Breadboard as indicated
![BuckPuckTop](pics/2_BuckPuck_top.JPG)

3) Turn the breadboard (with the BuckPuck) around and solder the pins 
to the breadboard. 
![BuckPuck Soldered](pics/3_BuckPuck_soldered.JPG)

4) Now take the 9V Battery Clip connector. Clip away the barrel plug 
and remove a little bit (2-5mm) of the cable protector. Put it into
the breadboard so that the read cable connects to **VIN** and the black 
cable connects to **GND**.
![Power source breaboard](pics/4_power_source.JPG)

5) Next, take one of the male barrel plugs and thread it through the 
opening on the side of the casing as indicated in the picture.
![Barrel Plug through casing](pics/5_barrel_plug_through_casing.JPG)

6) Plug the red wire of the barrel plug into the breadboad so that it
is connected to the **LED+** of the BuckPuck and the black cable to the 
**LED-**. Then solder it to the breadboard.
![Barrel Plug soldered](pics/6_barrel_soldered.JPG)

7) Now to the switch - take two jumper wires and cut them and strip 
2-5mm of the protective sheathing. 
![Cut jumper wires](pics/7_switch_cut_jumpers.JPG)

8) Connect each of the jumper wires to one of the two hooks. Solder it.
![Connected jumper with switch](pics/8_jumper_connected_to_switch.JPG)

9) Connect one of the jumper wires to the **DIM** and the other one to 
the **VIN**. Solder the jumper wire to the breadboard
![Swtich on breadboard](pics/9_switch_in_breadboard.JPG)

10) Turn the casing around and place the M3 nuts in the openings. You 
might need to hammer them in. Then turn the casing around again and 
place the breadboard into the casing and fasten it with the M3 screws.
![Backside casing](pics/10_backside_casing.JPG)

11) After fixing the breadboard unscrew the nut (and the other two 
pieces) on top of the switch and thread
it through the other opening in the casing. Fasten it again.
![Fixed breaboard and switch](pics/11_switch_breadboard_screwed.JPG)

12) Clip the 9V battery on the adapater and place it into the casing. 
Slide the cover on the casing to close it.
**Congratulations, the first part is done**  
![Casing Full](pics/12_casing_full.JPG)

13) Now the LED self is prepared - make sure you have all the components:
    - the 4 3D printed parts,
    - the heat sink
    - an LED
    - the lens and lens holder
    - 4x M3 screw and nut
    - one femaile barrel plug
    
    If you wish to also make an extension cable you additionally need: 
    - one female and one male barrel plug
    - cable for as long as you wish to make the extension cable
![LED compontents](pics/13_LED_components.JPG) 

14) Remove the protector of the heat sink and place the LED **as central
as you can** on the heatsink! Press down the LED (press on the sides,
not on the center!) 
![LED attached](pics/14_LED_attached.JPG)

15) Solder one of the female barrel plugs on the LED. Make sure to 
solder the red wire to the solder plate indicated with a (+) and the 
black wire to the solder plate indicated with a (-)
![LED soldered](pics/15_LED_soldered.JPG)

16) Prepare the LED holder - first press the M3 nuts in the openings of
the elongated part. Then push the M3x10 screws through the openings
of the flat part
![prepare LED holder 1](pics/16_prepare_holder1.JPG)

17) Place the lens on the LED (there is one particular orientation that
fits best, see picture!). Then place the heat sink on the prepared LED
hoder
![prepare LED holder 2](pics/17_prepare_holder2.JPG)

18) Screw the top plate into the long pieces. You should fasten the screws
well to make sure the LED doesn't pop out of the holder!
![LED holder finished](pics/18_LED_holder_finished.JPG)

19) You may now connect the LED to the LED controller using the barrel
plug. Test if you can turn the LED on or off
![First LED test](pics/19_first_LED_test.JPG)

###**Congratulations: Your essentially done**
If you want to learn how to make an extension cable, keep reading!

### Extension cable

1) For the extension cable you will need the following components:
- a male and female barrel plug
- cable with desired length
- some heat shrink tubes
![Extension cable components](pics/20_extension_cable_components.JPG)

2) After cutting the cable to the desired length, clip of the protective
sheathing for 2-5mm. Then put some heat shrink tubing on the cable.
![Extension cable 1](pics/21_extension_cable_1.JPG)

3) Solder the red wire of the barrel plug to one of the extension 
cables. Ideally red to red. If you have  a different color, remember 
which one was connected to the red one!
![Extension cable 2](pics/22_extension_cable_2.JPG)

4) Put the heat shrinking tube over the bare cable. Use a hot air gun 
to fix the heat shrinking tube into its place
![heat shrinking](pics/23_tube_shrinking.JPG)

5) It good for the stability of the cable to then put another, larger,
tube on top of the smaller ones.
![2nd heat shrink](pics/24_second_heat_shrink.JPG)

6) Do the same for the other side of the cable with the female barrel 
plug. **Make sure to put the heat shrink cable on the cable before 
soldering!**
![extension cable finished](pics/25_extension_cable_finished.JPG)

7) **Congratulations - you are done**
![finally done](pics/26_finally_done.JPG)



 ## Necessary tools
 
 - A wire [clipper/stripper](https://www.amazon.com/Cutter-Stripper-Stranded-Klein-Tools/dp/B00080DPNQ/ref=sr_1_24?keywords=cable+stripping+tool&qid=1560916342&s=gateway&sr=8-24)
 ![wire stripper](pics/Tools_stripper.JPG)
 - Soldering [Iron and ventilator
 ](https://www.amazon.com/T18-D08-D12-D24-D32-S3/dp/B00C1N30DI/ref=sr_1_16?keywords=soldering+station&qid=1560916600&s=gateway&sr=8-16) 
 (solder is **toxic!**)
 ![solderin iron](pics/Tools_soldering.JPG)
 - Some [solder wire](https://www.amazon.com/WYCTIN-Solder-Electrical-Soldering-0-11lbs/dp/B071G1J3W6/ref=sr_1_2?keywords=fine+solder+wire&qid=1560916706&s=gateway&sr=8-2)
 ![solder](pics/Tools_solder.JPG)
 - An M2.5 allen [wrench](https://www.amazon.com/dp/B071NBGBQD/ref=sspa_dk_detail_2?psc=1&pd_rd_i=B071NBGBQD&pd_rd_wg=kmByc&pd_rd_r=H240CYDM209AMVWTG5PS&pd_rd_w=9dkZE)
 ![hex screwdriver](pics/Tools_hex.JPG)